﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Umbraco.Core.Models;
using Wulvern.Web.ExtensionMethods;

namespace Wulvern.Web.Helpers
{
    public class DateHelper
    {
        public static string getOrdinal(DateTime date)
        {
            var num = date.Day;

            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }

        public static string getDateEventShort(DateTime date)
        {
            return getOrdinal(date) + " " + date.ToString("MMMM yyyy");
        }

        public static string getDateEventLong(DateTime date)
        {
            return date.ToString("dddd") + " " + getOrdinal(date) + " " + date.ToString("MMMM yyyy");
        }

        public static string getDateJavascript(DateTime date)
        {
            return date.ToString("MM") + "/" + date.ToString("dd") + "/" + date.ToString("yy");
        }

        public static string getDateJavascriptFullYear(DateTime date)
        {
            return date.ToString("MM") + "/" + date.ToString("dd") + "/" + date.ToString("yyyy");
        }

        
    }

    public class DateOrderingClass : IComparer<IPublishedContent>
    {
        public int Compare(IPublishedContent x, IPublishedContent y)
        {
            try
            {
                System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.CurrentCulture;
                
                /*
                DateTime dx = DateTime.ParseExact(x.GetProperty("date").Value.ToString().Substring(0, 10), dateFormat, provider);
                DateTime dy = DateTime.ParseExact(y.GetProperty("date").Value.ToString().Substring(0, 10), dateFormat, provider);
                */
                DateTime dx = System.Xml.XmlConvert.ToDateTime(x.GetProperty("publishedDate").Value.ToString());
                DateTime dy = System.Xml.XmlConvert.ToDateTime(y.GetProperty("publishedDate").Value.ToString());
                //int compareDate = dx.CompareTo(dy);
                int compareDate = dy.CompareTo(dx);
                                
                if (compareDate == 0)
                {
                    return 1;
                }
                return compareDate;
            }
            catch (Exception e)
            {
                e.ToString();
                return 1;
            }
        }
    }

    public class DownloadOrderingClass : IComparer<IPublishedContent>
    {
        public int Compare(IPublishedContent x, IPublishedContent y)
        {
            try
            {
                System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.CurrentCulture;

                /*
                DateTime dx = DateTime.ParseExact(x.GetProperty("date").Value.ToString().Substring(0, 10), dateFormat, provider);
                DateTime dy = DateTime.ParseExact(y.GetProperty("date").Value.ToString().Substring(0, 10), dateFormat, provider);
                */
                DateTime dx = System.Xml.XmlConvert.ToDateTime(x.GetProperty("uploadDate").Value.ToString());
                DateTime dy = System.Xml.XmlConvert.ToDateTime(y.GetProperty("uploadDate").Value.ToString());
                //int compareDate = dx.CompareTo(dy);
                int compareDate = dy.CompareTo(dx);

                if (compareDate == 0)
                {
                    return 1;
                }
                return compareDate;
            }
            catch (Exception e)
            {
                e.ToString();
                return 1;
            }
        }
    }
}