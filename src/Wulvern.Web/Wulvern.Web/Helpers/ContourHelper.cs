﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Services;
using Umbraco.Forms.Data.Storage;
using Wulvern.Web.Helpers;

namespace Wulvern.Web.Helpers
{
    public class ContourHelper
    {
        public ContourHelper() { }
        public static XPathNodeIterator PossibleAnswers(string guid)
        {
            FormStorage fs = new FormStorage();

            Form f = fs.GetForm(new Guid(guid));
            fs.Dispose();

            XmlDocument d = new XmlDocument();
            XmlNode root = d.CreateElement("questions");

            foreach (Field fld in f.AllFields)
            {

                if (fld.FieldType.SupportsPrevalues
                    && fld.PreValueSource.Type.GetPreValues(fld).Count > 0)
                {
                    XmlNode question = d.CreateElement("question");
                    XmlNode caption = d.CreateElement("caption");

                    XmlCDataSection questionvalue = d.CreateCDataSection(fld.Caption);
                    caption.AppendChild(questionvalue);

                    question.AppendChild(caption);

                    XmlNode answers = d.CreateElement("answers");

                    foreach (PreValue pv in fld.PreValueSource.Type.GetPreValues(fld))
                    {
                        XmlNode answer = d.CreateElement("answer");
                        XmlAttribute answerId = d.CreateAttribute("id");
                        answerId.Value = pv.Id.ToString();
                        answer.Attributes.Append(answerId);
                        XmlCDataSection value = d.CreateCDataSection(pv.Value);
                        answer.AppendChild(value);
                        answers.AppendChild(answer);
                    }

                    question.AppendChild(answers);
                    root.AppendChild(question);
                }

            }

            d.AppendChild(root);

            return d.CreateNavigator().Select(".");
        }

        public class SetCookieOnSubmit : umbraco.BusinessLogic.ApplicationBase
        {

            public SetCookieOnSubmit()
            {
                RecordService.RecordSubmitted +=
                    new EventHandler<Umbraco.Forms.Core.RecordEventArgs>(RecordService_RecordSubmitted);
            }

            void RecordService_RecordSubmitted(object sender, Umbraco.Forms.Core.RecordEventArgs e)
            {
                umbraco.library.setCookie(e.Form.Id.ToString(), "1");
            }


        }
    }
}