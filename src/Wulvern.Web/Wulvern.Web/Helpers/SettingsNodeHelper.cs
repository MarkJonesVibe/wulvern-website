﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.MacroEngines;
using umbraco.presentation.nodeFactory;

namespace Wulvern.Web.Helpers
{
    public class SettingsNodeHelper
    {
        public string GetSettingsPropertyValue(string propertyName)
        {
            Node settingsNode = new Node(1062);
            return settingsNode.GetProperty(propertyName).Value.ToString();
        }
    }
}