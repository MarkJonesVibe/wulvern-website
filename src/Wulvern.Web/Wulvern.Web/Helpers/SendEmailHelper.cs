﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using Umbraco.Core.Services;
using Wulvern.Infrastructure.Models;
using Wulvern.Web.Models;

namespace Wulvern.Web.Helpers
{
    public class SendEmailHelper
    {
        public bool SendEmail(JobApplicationModel model, HttpPostedFileBase CVUpload, HttpPostedFileBase CoveringLetterUpload)
        {
            ContentService cs = new ContentService();
            try
            {
                //get to from settings node
                var defaultEmail = cs.GetById(1062).GetValue("defaultEmail").ToString();

                var usmtphost = System.Configuration.ConfigurationManager.AppSettings["usmtphost"];
                var usmtpsername = System.Configuration.ConfigurationManager.AppSettings["usmtpsername"];
                var usmtppassword = System.Configuration.ConfigurationManager.AppSettings["usmtppassword"];

                using (MailMessage mail = new MailMessage(usmtpsername, defaultEmail))
                {
                    string body = String.Format("Job Id: {0} <br/> Job Title: {1} <br /> Name: {2} <br/> Email: {3} <br /> Telephone: {4}", model.JobId, model.JobTitle, model.Firstname + " " + model.Surname, model.Email, model.Telephone);
                    mail.Subject = "Job Application";
                    mail.Body = body;

                    // Pul the file stream from the File uploader and both streams as the attachments
                    mail.Attachments.Add(new Attachment(CVUpload.InputStream, CVUpload.FileName));
                    mail.Attachments.Add(new Attachment(CoveringLetterUpload.InputStream, CoveringLetterUpload.FileName));

                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = usmtphost;
                    smtp.EnableSsl = true;
                    NetworkCredential networkCredential = new NetworkCredential(usmtpsername, usmtppassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = networkCredential;
                    smtp.Port = 587;
                    smtp.Send(mail);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool QuickContactdEmail(ContactFormModel model)
        {
            ContentService cs = new ContentService();
            try
            {
                var usmtphost = System.Configuration.ConfigurationManager.AppSettings["usmtphost"];
                var usmtpsername = System.Configuration.ConfigurationManager.AppSettings["usmtpsername"];
                var usmtppassword = System.Configuration.ConfigurationManager.AppSettings["usmtppassword"];

                //get to from settings node
                var defaultEmail = cs.GetById(1062).GetValue("defaultEmail").ToString();

                using (MailMessage mail = new MailMessage(usmtpsername, defaultEmail))
                {
                    string body = String.Format("Name: {0} <br/> Email: {1} <br /> Telephone: {2}", model.FirstName, model.Surname, model.Phone);
                    mail.Subject = "Job Application";
                    mail.Body = body;

                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = usmtphost;
                    smtp.EnableSsl = true;
                    NetworkCredential networkCredential = new NetworkCredential(usmtpsername, usmtppassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = networkCredential;
                    smtp.Port = 587;
                    smtp.Send(mail);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool newsLetterEmail(string emailAddress)
        {
            ContentService cs = new ContentService();
            try
            {
                var usmtphost = System.Configuration.ConfigurationManager.AppSettings["usmtphost"];
                var usmtpsername = System.Configuration.ConfigurationManager.AppSettings["usmtpsername"];
                var usmtppassword = System.Configuration.ConfigurationManager.AppSettings["usmtppassword"];

                //get to from settings node
                var defaultEmail = cs.GetById(1062).GetValue("defaultEmail").ToString();

                using (MailMessage mail = new MailMessage(usmtpsername, defaultEmail))
                {
                    string body = String.Format("Email: {0} <br/>", emailAddress);
                    mail.Subject = "Newsletter Reigstration";
                    mail.Body = body;

                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = usmtphost;
                    smtp.EnableSsl = true;
                    NetworkCredential networkCredential = new NetworkCredential(usmtpsername, usmtppassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = networkCredential;
                    smtp.Port = 587;
                    smtp.Send(mail);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}