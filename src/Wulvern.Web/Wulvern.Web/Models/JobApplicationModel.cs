using System.ComponentModel.DataAnnotations; 
namespace Wulvern.Web.Models
{ 
	public class JobApplicationModel
	{
		[Required]
        [Display(Name = "First Name *")]
		public string Firstname {get; set;}

		[MaxLength(50)]
        [Display(Name = "Surname *")]
		public string Surname {get; set;}

		[Required]
        [Display(Name = "Email *")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
		public string Email {get; set;}

		[Required]
        [Phone(ErrorMessage = "Please enter a valid phone number")]
        [Display(Name = "Telephone *")]
		public string Telephone {get; set;}

		[Required]
        [Display(Name = "Upload your CV *")]
		public string CVUpload {get; set;}

        [Required]
        [Display(Name = "Upload your covering letter *")]
		public string CoveringLetterUpload {get; set;}

        public string JobId { get; set; }
        public string JobTitle { get; set; }
	}
}