/**
 * $Id: editor_plugin_src.js 677 2008-03-07 13:52:41Z spocke $
 *
 * @author Moxiecode
 * @copyright Copyright � 2004-2008, Moxiecode Systems AB, All rights reserved.
 */

(function () {
    tinymce.PluginManager.requireLangPack('umbraco');

    tinymce.create('tinymce.plugins.UmbracoAutoTagPlugin', {
        init: function (ed, url) {
            ed.onChange.add(function () {
                if (typeof (RequestTags) == 'function') {
                    ed.save();
                    if (autoTags.generateTagsAutomatically != 'False') {
                        $('div.tags li').remove();
                        RequestTags();
                    }
                }

            });
        },

        getInfo: function () {
            return {
                longname: 'Umbraco Auto tags',
                author: 'Umbraco',
                authorurl: 'http://umbraco.org',
                infourl: 'http://umbraco.org',
                version: "1.0"
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add('autotags', tinymce.plugins.UmbracoAutoTagPlugin);

})();