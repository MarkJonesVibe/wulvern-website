<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:ps="urn:schemas-percipient-studios"
	xmlns:umbraco.library="urn:umbraco.library"
	xmlns:contourpoll="urn:contourpoll"
	xmlns:umbraco.contour="urn:umbraco.contour"
	exclude-result-prefixes="msxml umbraco.library contourpoll">
	<msxml:using namespace="umbraco.contour" />

<xsl:output method="xml" omit-xml-declaration="yes"/>
		


<xsl:param name="currentPage"/>
 
<xsl:param name="formid" select="/macro//formid" />
    
<xsl:template match="/">
  
    <xsl:if test="$formid">
     <xsl:variable name="records" select="umbraco.contour:GetRecordsFromForm($formid)"/>
     <xsl:variable name="numberOfRecords" select="count($records/uformrecord)" />
    
   <div class="results" style="width:300px">
       <h3>Poll results</h3>
     
     <xsl:for-each select="ps:PossibleAnswers($formid)//question">
    <xsl:variable name="numberOfAnswers" select="count(./answer)" />
       
    <h4><xsl:value-of select="caption"/></h4>
      <ul style="list-style:none;padding-left:0px;margin-left:0px;">
        <xsl:for-each select=".//answer">
      <xsl:sort select="count($records//value [@key = current()/@id])"
                data-type="number" order="descending"/>
 
      <xsl:variable name="currentId" select="@id" />
      <xsl:variable name="currentVotes" select="count($records//value [@key = $currentId])" />
      <xsl:variable name="currentPercentage"
                        select="round (($currentVotes div $numberOfRecords) * 100)"/>
      
      <li>
          <xsl:value-of select="."/>
          <div class="scorebarcontainer" style="width:100%;background-color:#eee;height:20px">
            <div class="scorebar"
                    style="width:{$currentPercentage}%;background-color:#ccc;height:20px">
            </div>
          </div>
      </li>
   </xsl:for-each>
  </ul>
       </xsl:for-each>
    </div>
      
    </xsl:if>
</xsl:template>
		
<!-- =========================================================== -->
 
<msxml:script language="CSharp" implements-prefix="ps">
<msxml:using namespace="System.IO" />
<msxml:assembly name="System.Web" />
<msxml:using namespace="System.Web" />
<msxml:assembly name="Umbraco.Forms.Core" />
<msxml:using namespace="Umbraco.Forms.Core" />
<msxml:assembly name="Umbraco.Forms.Core" />
<msxml:using namespace="Umbraco.Forms.Core.Services" />
<msxml:assembly name="Umbraco.Forms.Core" />
<msxml:using namespace="Umbraco.Forms.Data.Storage" />
<![CDATA[
 public static XPathNodeIterator PossibleAnswers(string guid)
        {
            FormStorage fs = new FormStorage();
 
            Form f = fs.GetForm(new Guid(guid));
            fs.Dispose();
 
            XmlDocument d = new XmlDocument();
            XmlNode root = d.CreateElement("questions");
 
            foreach (Field fld in f.AllFields)
            {
 
                if (fld.FieldType.SupportsPrevalues 
                    && fld.PreValueSource.Type.GetPreValues(fld).Count > 0)
                {
                    XmlNode question = d.CreateElement("question");
                    XmlNode caption = d.CreateElement("caption");
 
                    XmlCDataSection questionvalue = d.CreateCDataSection(fld.Caption);
                    caption.AppendChild(questionvalue);
 
                    question.AppendChild(caption);
 
                    XmlNode answers = d.CreateElement("answers");
 
                    foreach (PreValue pv in fld.PreValueSource.Type.GetPreValues(fld))
                    {
                        XmlNode answer = d.CreateElement("answer");
                        XmlAttribute answerId = d.CreateAttribute("id");
                        answerId.Value = pv.Id.ToString();
                        answer.Attributes.Append(answerId);
                        XmlCDataSection value = d.CreateCDataSection(pv.Value);
                        answer.AppendChild(value);
                        answers.AppendChild(answer);
                    }
 
                    question.AppendChild(answers);
                    root.AppendChild(question);
                }
 
            }
 
            d.AppendChild(root);
 
            return d.CreateNavigator().Select(".");
        }
]]>
</msxml:script>
 
<!-- =========================================================== -->
		
	
</xsl:stylesheet>