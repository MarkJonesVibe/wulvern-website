﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Wulvern.Infrastructure.Models;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace Wulvern.Web.Controllers.SurfaceControllers
{
    public class SendEmailSurfaceController : SurfaceController
    {
        [HttpPost]
        [ChildActionOnly]
        public ActionResult SendEmail(CVUploadModel model, HttpPostedFileBase fileUploader)
        {
            if (ModelState.IsValid)
            {
                string from = "Your Gmail Id"; //example:- sourabh9303@gmail.com
                using (MailMessage mail = new MailMessage(from, System.Configuration.ConfigurationManager.AppSettings["jobApplicationEmail"]))
                {
                    mail.Subject = "Job Application";
                    mail.Body = "CV Upload Body";
                    if (fileUploader != null)
                    {
                        string fileName = Path.GetFileName(fileUploader.FileName);
                        mail.Attachments.Add(new Attachment(fileUploader.InputStream, fileName));
                    }
                    mail.IsBodyHtml = false;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.Office365.com";
                    smtp.EnableSsl = true;
                    NetworkCredential networkCredential = new NetworkCredential(from, "bacK56Nv");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = networkCredential;
                    smtp.Port = 587;
                    smtp.Send(mail);
                    ViewBag.Message = "Sent";
                    return CurrentUmbracoPage();
                }
            }
            else
            {
                return View();
            }
        }
    }
}