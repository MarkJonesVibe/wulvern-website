﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;
using Wulvern.Web.Helpers;
using Wulvern.Infrastructure.Models;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Wulvern.Web.Controllers.SurfaceControllers
{
    public class ContactFormSurfaceController : SurfaceController
    {
        [HttpPost]
        public ActionResult Send(ContactFormModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    // Removed this line as dont think we need to save Doc to another location
                    //file.SaveAs(Path.Combine(Server.MapPath("~/UploadedCV"), fileName));

                    ModelState.Clear();
                    // Have removed Send Email from controller responsibility and added as helper method
                    var mailHelper = new SendEmailHelper();
                    if (mailHelper.QuickContactdEmail(model))
                        ViewBag.Message = "CV Upload Successful";
                    else
                        ViewBag.Message = "CV Upload Failed";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Error! Please try again";
                }
            }
           
            return CurrentUmbracoPage();
        }
    }
}