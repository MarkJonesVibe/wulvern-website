﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Wulvern.Infrastructure.Models;
using ReachmailApi;
using ReachmailApi.Lists.Recipients.Subscribe.ByListId.Post.Request;

using Wulvern.Web.Helpers;

namespace Wulvern.Web.Controllers.SurfaceControllers
{
    public class NewsletterSubscriptionSurfaceController : SurfaceController
    {
        [HttpPost]
        [ActionName("Subscribe")]
        public void Subscribe()
        {
            var email = Request["signup"];
            //var reachmail = Reachmail.Create("wulvern", "admin", "Crewe1990!");
            var mailHelper = new SendEmailHelper();
            mailHelper.newsLetterEmail(email);
 
            Response.Redirect("/newsletter-signup.aspx");
        }
    }
}