using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Wulvern.Web.Helpers;
using Wulvern.Web.Models;


namespace Wulvern.Web.Controllers.SurfaceControllers
{ 
    public class JobApplicationSurfaceController : SurfaceController
    {
		//Method for rendering partial view with @Html.Action("RenderJobApplication","JobApplicationSurface");
		[ChildActionOnly]
        public ActionResult RenderJobApplication(){
            return PartialView("JobApplication", new JobApplicationModel());
		}

		//endpoint for posting data to a surface controller action
	    [HttpPost]
        public ActionResult HandleJobApplication(JobApplicationModel model)
        {
            var CVUpload = Request.Files["CVUpload"];
            var CoveringLetterUpload = Request.Files["CoveringLetterUpload"];
            
            if (!(CVUpload.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                CVUpload.ContentType == "application/pdf"))
            {
                ModelState.AddModelError("CVUpload", "Only .docx and .pdf file allowed");
            }

            if (!(CoveringLetterUpload.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                CoveringLetterUpload.ContentType == "application/pdf"))
            {
                ModelState.AddModelError("CoveringLetterUpload", "Only .docx and .pdf file allowed");
            }

			//model not valid, do not save, but return current umbraco page
			if (!ModelState.IsValid)
			{
				//Perhaps you might want to add a custom message to the TempData or ViewBag
				//which will be available on the View when it renders (since we're not 
				//redirecting)          
				return CurrentUmbracoPage();
			}
            
			//if validation passes perform whatever logic
			//In this sample we keep it empty, but try setting a breakpoint to see what is posted here

            if (CurrentPage.DocumentTypeAlias == "Vacancy")
            {
                model.JobTitle = CurrentPage.GetProperty("title").Value.ToString();
                model.JobId = CurrentPage.GetProperty("jobId").Value.ToString();
            }
            else
            {
                model.JobTitle = "Generic";
                model.JobId = "-";
            }
            // string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
            // Removed this line as dont think we need to save Doc to another location
            //file.SaveAs(Path.Combine(Server.MapPath("~/UploadedCV"), fileName));

            //model.CV = cvFile.InputStream;
            //model.CVName = cvFile.FileName;

            //model.CoverLetter = clFile.InputStream;
            //model.CoverLetterName = clFile.FileName;

            ModelState.Clear();
            // Have removed Send Email from controller responsibility and added as helper method
            var mailHelper = new SendEmailHelper();
            if (mailHelper.SendEmail(model, CVUpload, CoveringLetterUpload))
            {
                TempData.Add("CustomMessage", "Your application was successfully submitted at " + DateTime.Now);
                return RedirectToUmbracoPage(1953);
            }
            else
            {
                TempData.Add("CustomMessage", "There was a problem submitting your application");
            }
            
			//Perhaps you might want to store some data in TempData which will be available 
			//in the View after the redirect below. An example might be to show a custom 'submit
			//successful' message on the View, for example:
            

			//redirect to current page to clear the form
			return RedirectToCurrentUmbracoPage();

			//Or redirect to specific page
            //return RedirectToUmbracoPage(1953);
        }
	}
}
