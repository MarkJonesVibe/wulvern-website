﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Wulvern.Web.Controllers.SurfaceControllers
{
    public class NavigationSurfaceController : SurfaceController
    {
        [OutputCache(Duration = 100000)]
        public PartialViewResult NavMenu()
        {
            return PartialView("DesktopNavigation");
        }
    }
}