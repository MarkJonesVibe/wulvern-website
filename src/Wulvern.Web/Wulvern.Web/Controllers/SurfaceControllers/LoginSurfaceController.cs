﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Wulvern.Infrastructure.Models;

namespace Wulvern.Web.Controllers.SurfaceControllers
{
    public class LoginSurfaceController : SurfaceController
    {
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (model.Email == null || model.Password == null)
                return CurrentUmbracoPage();

            // We can use this method when AD is available to aurthenticate against, we just need to change the provider to AD in web.config
            if (Membership.ValidateUser(model.Email, model.Password))
            {
                // Will replace number of redirect for it to pull from page name eentually stored in settings
                // Will just redirect to the about us page for now
                // Success, create non-persistent authentication cookie.
                FormsAuthentication.SetAuthCookie(
                        model.Email.Trim(), false);

                FormsAuthenticationTicket ticket1 =
                   new FormsAuthenticationTicket(
                        1,                                   // version
                        model.Email.Trim(),   // get username  from the form
                        DateTime.Now,                        // issue time is now
                        DateTime.Now.AddMinutes(10), true, "userData"   // expires in 10 minutes

                    // in userData
                        );
                HttpCookie cookie1 = new HttpCookie(
                  FormsAuthentication.FormsCookieName,
                  FormsAuthentication.Encrypt(ticket1));
                Response.Cookies.Add(cookie1);

                // 4. Do the redirect. 
                String returnUrl1;
                // the login is successful
                if (Request.QueryString["ReturnUrl"] == null)
                {
                    returnUrl1 = "~/";
                }

                //login not unsuccessful 
                else
                {
                    returnUrl1 = Request.QueryString["ReturnUrl"];
                }
                Response.Redirect(returnUrl1);
            }

            return CurrentUmbracoPage();
        }
    

        [HttpGet]
        [ActionName("Logout")]
        public void Logout()
        {
            Response.Redirect("~/");
        }
    }
}