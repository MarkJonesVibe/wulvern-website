﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Wulvern.Infrastructure.Models;
using Wulvern.Web.Helpers;
using System.IO;

namespace Wulvern.Web.Controllers.SurfaceControllers
{
    public class CVUploadSurfaceController : SurfaceController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitCV(CVUploadModel model, HttpPostedFileBase cvFile, HttpPostedFileBase clFile, string jobTitle, string jobId)
        {
            if (cvFile == null)
            {
                ModelState.AddModelError("CustomError", "Please select CV");
                return CurrentUmbracoPage();
            }

            if (!(cvFile.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                cvFile.ContentType == "application/pdf"))
            {
                ModelState.AddModelError("CustomError", "Only .docx and .pdf file allowed");
                return CurrentUmbracoPage();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    model.JobTitle = "Domestic Technicians";
                    model.JobId = "jobId_1";
                    // string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    // Removed this line as dont think we need to save Doc to another location
                    //file.SaveAs(Path.Combine(Server.MapPath("~/UploadedCV"), fileName));
                    model.CV = cvFile.InputStream;
                    model.CVName = cvFile.FileName;

                    model.CoverLetter = clFile.InputStream;
                    model.CoverLetterName = clFile.FileName;

                    ModelState.Clear();
                    // Have removed Send Email from controller responsibility and added as helper method
                    var mailHelper = new SendEmailHelper();
                    /*
                    if (mailHelper.SendEmail(model))
                        ViewBag.Message = "CV Upload Successful";
                    else
                        ViewBag.Message = "CV Upload Failed";
                     * */
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Error! Please try again";
                    return CurrentUmbracoPage();
                }

                
            }
            return CurrentUmbracoPage();
        }
    }
}