﻿$().ready(function () {
    serialiseTags();
    $('input.tags').autoGrowInput({
        comfortZone: 5,
        minWidth: 10,
        maxWidth: 200
    });


    $('#' + autoTags.clearButton).click(function (e) {
        if (confirm("You are about to remove all tags, do you wish to continue?")) {
            $('div.tags li').remove();
            serialiseTags();
        }
        e.preventDefault();
        return false;
    });

    $(document).bind("contextmenu", function (e) {
        return false;
    });

    $('div.tagsContainer > div').click(function (eve) {
        var n = eve.target.nodeName.toLowerCase();
        if (n == 'div' || n == 'ul') {

            addTag();
            eve.preventDefault();
            return false;
        }
    });

    function addTag() {
        if (autoTags.allowUserTags != 'False') {
            $('div.tags ul').append('<li class="active user"><input class="tags active user" type="text" name="gtTags_' + $('div.tags li').size() + '" value="" size="1" style="width: 25px;"/> <span>x</span></li>');

            $('input.tags').autoGrowInput({
                comfortZone: 5,
                minWidth: 10,
                maxWidth: 200
            });

            $('input.tags:last').focus();
        }
    }

    $('.tags a').live('click', function () {
        if ($(this).attr('id').indexOf('body_TabView') > -1) {
            $('input.tags').autoGrowInput({
                comfortZone: 5,
                minWidth: 10,
                maxWidth: 200
            });
        }
    });

    $('input.tags').live('keypress', function (e) {
        if (e.keyCode === 9) {
            addTag();
            e.preventDefault();
            return false;
        }
    });

    $('input.tags').live('blur', function (e) {
        serialiseTags();
    });

    // Only hook up events if generateTagsAutomatically is set
    if (autoTags.generateTagsAutomatically != 'False') {

        $('form textarea').blur(function () {
            $('div.tags li').remove();
            RequestTags();
        });

        $('form input:not(.tags ul li input, .tagsNotification input)').live('blur', function () {
            $('div.tags li').remove();
            RequestTags();
        });
    }

    function saveNotification() {
        $('div.autoTagsStatus').html('<img src="' + autoTags.spinnerImage + '" width="16" height="16"/> Saving tags');
    }

    $('div.tags ul li span').live('click', function () {
        $(this).parent().fadeOut(300, function () { $(this).remove(); });
        serialiseTags();
    });

    $('div.tags ul li').live('mousedown', function (e) {

        if (e.button == 2) {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('input', this).removeClass('active');
            } else {
                $(this).addClass('active');
                $('input', this).addClass('active');
            }

            e.preventDefault();
            serialiseTags();
            return false;
        }
    });


    $('#' + autoTags.tagbutton).click(function () {
        RequestTags();

        return false;
    });
});

function TagInList(list, tag) {

    var i = 0;
    for (i = 0; i < list.length; i++) {
        if ($.trim($(list[i]).val().toLowerCase()) == $.trim(tag.toLowerCase())) {
            return true;
        }
    }

    return false;
}

function isNullOrEmpty(obj) {
    return (obj == null || obj == undefined || obj == '');
}

function RequestTags() {
    $('div.autoTagsStatus').html('<img src="' + autoTags.spinnerImage + '" width="16" height="16"/> Getting tags');

    $('div.autoTagsStatus').show();
    var postData = '';

    if (!isNullOrEmpty(autoTags.aliasesToUse)) {
        addClassToFormElementsWithIds(autoTags.aliasesToUse.split(','), 'autotags-include');
        postData = $('form .autotags-include :not(input[name^="__"])').fieldSerialize();
    }
    else if (!isNullOrEmpty(autoTags.aliasesToExclude)) {
        addClassToFormElementsWithIds(autoTags.aliasesToExclude.split(','), 'autotags-exclude');
        postData = $('form :not(.autotags-exclude) :not(input[name^="__"])').fieldSerialize();
    } else {
        postData = $('form :not(input[name^="__"])').serialize();
    }

    autoTags.requests++;

    var requestType = '';
    if ($('.multiNodePicker').length > 0) {
        requestType = 'GET';
    } else {
        requestType = 'POST';
    }

    $.ajax({
        type: requestType,
        url: '/base/AutoTags/GetTags/' + autoTags.nodeId + '.aspx',
        data: postData,
        cache: false,
        success: function (data) {
            autoTags.requests--;
            if (PopulateTags(data)) {

                $('div.autoTagsStatus').hide();
            }

        },
        error: function() { autoTags.requests--; },
        dataType: 'json'
    });
}

function addClassToFormElementsWithIds(collectionOfIds, classToAdd) {
    for (i in collectionOfIds) {
        var ele = $('form *[id$="' + collectionOfIds[i] + '"]');
        ele.addClass(classToAdd);
    }
}

function serialiseTags() {
    var tags = new Array();
    $('div.tags ul li input.active').each(function () {
        var v = $(this).val();
        if (v.length > 0) {
            tags.push(v);
        }
    });

    $('#' + autoTags.hiddenField).val(tags.join(','));

}

function PopulateTags(data) {

    if (data.error) { $('div.autoTagsStatus').html(data.error); return false; }

    var res = data;
    if (res == null || res.items == null) { return false; }

    var keys = new Array();
    $(res.items).each(function () {
        keys.push(this);
    });

    if (keys.length == 0) {
        $('div.autoTagsStatus').html('No tags could be genereated for the provided content');

        $('div.autoTagsStatus').show();

        return false;
    }

    keys.sort();

    var selected = res.autoAdd == true ? ' active' : '';

    var i = 0;
    $(keys).each(function () {
        var items = $('div.tags ul li input');

        if (!TagInList(items, this)) {
            var listElement = '';

            if (res.autoAdd) {
                listElement = '<li class="active"><input class="tags active suggested" type="text" name="gtTags_' + i + '" value="' + this + '"/> <span>x</span></li>'
            } else {
                listElement = '<li><input class="tags suggested" type="text" name="gtTags_' + i + '" value="' + this + '"/> <span>x</span></li>'
            }

            $('div.tags ul').append(listElement);
            i++;
        }
    });

    serialiseTags();

    $('input.tags').autoGrowInput({
        comfortZone: 5,
        minWidth: 10,
        maxWidth: 200
    });

    return true;
}
