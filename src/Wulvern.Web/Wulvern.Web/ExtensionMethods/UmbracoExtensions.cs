﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Umbraco.Core.Dynamics;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Wulvern.Web.ExtensionMethods
{
    public static class UmbracoExtensions
    {
        public static IEnumerable<IPublishedContent> GetMntpNodes(this IPublishedContent node, string propertyName)
        {
            var xml = node.GetPropertyValue<RawXmlString>(propertyName);

            if (xml != null)
            {
                var xmlData = xml.Value;

                if (!string.IsNullOrEmpty(xmlData))
                {
                    var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                    return umbracoHelper.TypedContent(XElement.Parse(xmlData).Descendants("nodeId").Select(x => (x.Value))).Where(y => y != null);
                }
            }

            return Enumerable.Empty<IPublishedContent>();
        }

        public static List<string> GetDownloadNodes(this IPublishedContent node, string propertyName)
        {
            List<string> downloadNodes = new List<string>();
            var xml = node.GetPropertyValue<RawXmlString>(propertyName);

            if (xml != null)
            {
                var xmlData = xml.Value;

                if (!string.IsNullOrEmpty(xmlData))
                {
                    var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                    return (XElement.Parse(xmlData).Descendants("nodeId").Select(x => (x.Value))).Where(y => y != null).ToList();
                }
            }

            return new List<string>();
        }

        public static IEnumerable<IPublishedContent> Paged(this IEnumerable<IPublishedContent> nodes, int page, int pageSize)
        {
            //return nodes.Skip((page) * pageSize).Take(pageSize);
            return nodes.Skip((page-1) * pageSize).Take(pageSize);
        }
               

        public static IEnumerable<IPublishedContent> FilterByCategory(this IPublishedContent node, string categoryName)
        {
            return Enumerable.Empty<IPublishedContent>();
        }
    }
}