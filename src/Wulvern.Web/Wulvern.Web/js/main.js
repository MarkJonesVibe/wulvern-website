var lastScrollTop 	= 0;
var headerAnimating = 0;
var headAnimTrigger = 20;

$(document).ready(function(){
	
	 $('.infield-label').infieldLabel();
	
	// jquery cycle 2 
	$('.slides').cycle({
		speed: 400,
		manualSpeed: 100,
		timeout:5000,
		slides: '> div',
		fx: 'scrollHorz',
		swipe: true,
		next:'#dslide-next',
		prev:'#dslide-previous',
		pager: '#dslidepager'
	});

	$('.mslides').cycle({
	    speed: 400,
	    manualSpeed: 100,
	    timeout: 5000,
	    slides: '> div',
	    fx: 'scrollHorz',
	    swipe: true,
	    next: '#mslide-next',
	    prev: '#mslide-previous',
	    pager: '#mslidepager'
	});
	
	$('.tweets').cycle({
		speed: 400,
		manualSpeed: 100,
		timeout:5000,
		slides: '> div',
		fx: 'scrollHorz',
		swipe: true,
		next:'.tweet-next',
		prev:'.tweet-previous',
		pager: '.pager'
		
	});
	
	// window less tham 768 so homes needs to be slideshow
   if ($(window).width() < 768) {
	 $('.homeslides').cycle({
		speed: 400,
		manualSpeed: 100,
		timeout:5000,
		slides: '> div',
		fx: 'scrollHorz',
		swipe: true,
		next:'.homes-next',
		prev:'.homes-previous',
		pager: '.homespager'
		
	});
   }
	
	// Navigation
	$('nav ul li').not("nav ul li div li").hover(
		function() {
		   $(this).children().next().show();
		}, 
		function() {
		   $(this).children().next().show();
		  $('.subNavContainer').hide();
		}
	);
	
	// icon rollovers
	$('.telIcon').hover(
		function() {
		   $('.telText').fadeIn( "fast", function() {});
		}, 
		function() {
		   $('.telText').hide();
		}
	);
	
	$('.emailIcon').hover(
		function() {
		   $('.emailText').fadeIn( "fast", function() {});
		}, 
		function() {
		   $('.emailText').hide();
		}
	);
	
	$('.searchButton').hover(
		function() {
			if ($(window).width() < 1024 || $( window ).scrollTop()>=headAnimTrigger) {
		    	$('.searchFieldContainer').fadeIn( "fast", function() {});
			}
		},
		function() {
			if ($(window).width() < 1024) {
		   		$('.searchFieldContainer').hide()( "fast", function() {});
			}
		}
	);
	
	$('.searchArea').mouseleave(function() {
	  if ($( window ).scrollTop()>=headAnimTrigger) {
	 	 $('.searchFieldContainer').fadeOut( "fast", function() {});
	  }
	});
	
	// mobile search toggle
	$( "#mobileSeatchToggle" ).click(function() {
	  $( ".mobileSearchContainer" ).fadeToggle( "fast", "linear" );
	});
	
    // Job form expander
    /*
	$( ".applyNow" ).click(function() {
		
		if ($(this).width()<643) {
			
			$(this).animate({
				width: "643px",
			}, 250, function() {
				// Animation complete.
				
				// change button to minus
				$(this).css('background-position', 'right -30px');
				
				// get the job id
				jobid = $(this).attr('id');
				
				jobid = jobid.replace('jobid_','')
				
				// get the form from the template and insert into page in appropriate location
				formTemplate = $('#appFormTemplate').html();
				
				formTemplate = formTemplate.replace('#jobid#',jobid)
				
				$(this).next(".jobAppForm").html(formTemplate);
				
				$(this).next(".jobAppForm").fadeIn();
				
			});
			
		} else {
			
			$(this).next(".jobAppForm").fadeOut();
			
			$(this).animate({
				width: "150px",
			}, 250, function() {
				// Animation complete.
				
				// chaneg button yo plus
				$(this).css('background-position', 'right 5px');
				
				$(this).next(".jobAppForm").empty();
				
			});
		}

	});
	*/
	// Header animation
	
    // Header animation

	$(window).scroll(function () {

	    //if ($(window).width() >= 1024) {
	    if ($("nav ul").css("display") == 'block' && !window.mobilecheck) {

	        var st = $(this).scrollTop();
	        if (st > lastScrollTop) {
	            // downscroll code
	            if ($(window).scrollTop() >= headAnimTrigger && headerAnimating == 0) {

	                headerAnimating = 1;

	                $(".fadeOut").hide();

	                $(".wulvernLogo").animate({
	                    width: "200px",
	                    marginTop: "-22px"
	                }, 250, function () {
	                    // Animation complete.
	                    headerAnimating = 0;
	                 
	                    $(".fadeIn").fadeIn("fast", function () { });
	                });

	                $("header").animate({
	                    height: "70px"
	                })

	                $(".placeholder-focus").css("display", "none");


	            }

	        } else {
	            // upscroll code
	            if ($(window).scrollTop() < headAnimTrigger && headerAnimating == 0) {

	                headerAnimating = 1;

	                $(".fadeIn").hide();

	                $(".wulvernLogo").animate({
	                    width: "265px",
	                    marginTop: "0px"
	                }, 250, function () {
	                    // Animation complete.
	                    headerAnimating = 0;
	                });

	                $("header").animate({
	                    height: "122px"
	                })

	                //$(".placeholder-focus label").css("display","block");

	                $(".fadeOut").fadeIn("fast", function () { });     

	            }
	        }

	        lastScrollTop = st;

	    }


	});

});

// Read more toggle
function toggleReadMore(id) {
	$('#'+id).toggle();
	$('.link_'+id).toggle();
}

//  jump to just above an anchor within page
//$(window).on("hashchange", function () {
//    window.scrollTo(window.scrollX, window.scrollY - 200);
//});


// handle windo wresize
$( window ).resize(function() {
	
	// window less tham 768 so homes needs to be slideshow
   if ($(window).width() < 768) {
	 $('.homeslides').cycle({
		speed: 400,
		manualSpeed: 100,
		timeout:5000,
		slides: '> div',
		fx: 'scrollHorz',
		swipe: true,
		next:'.homes-next',
		prev:'.homes-previous',
		pager: '.homespager'
		
	});
   }
   
    if ($(window).width() >= 768) {
		$('.homeslides').cycle('destroy');
	}
   
   
});

$(function () {
    if ($('.field-validation-error').length > 0) {
        var form = $('.field-validation-error').first().parents('form:first');
        var off = form.offset().top - 150;

        $("body").animate({
            scrollTop: off
        }, 0);
    }
});