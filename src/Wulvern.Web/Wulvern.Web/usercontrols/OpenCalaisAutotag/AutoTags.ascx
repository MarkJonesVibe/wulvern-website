<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoTags.ascx.cs" Inherits="GrailTechnology.AutoTags.DataType.AutoTags" %>
<script type="text/javascript">

    var autoTags = {};
    autoTags.tagbutton = '<%= buttonGetTags.ClientID %>'
    autoTags.nodeId = '<%= Request["id"] %>';
    autoTags.generateTagsAutomatically = '<%= GenerateAutomatically %>';
    autoTags.allowUserTags = '<%= AllowManualTags %>';

    autoTags.spinnerImage = '<%= SpinnerImage %>';
    autoTags.hiddenField = '<%= ActiveTags.ClientID %>';
    autoTags.clearButton = '<%= ClearTags.ClientID %>';
    autoTags.aliasesToUse = '<%= PropertyAliasesToUse %>';
    autoTags.aliasesToExclude = '<%= PropertyAliasesToExclude %>';
    autoTags.requests = 0;


</script>
<asp:Literal ID="Literal1" runat="server"></asp:Literal>
   
   <div class="tagsContainer" style="position:relative">
        <div class="tags" style="width: 500px; min-height: 20px; border: 1px solid #aaaaaa; background: white;">
            <ul>
                <asp:Literal ID="litExistingTags" runat="server" EnableViewState="false" />
            </ul>
            <div style="clear:both; height: 10px;"></div>

        </div>
       
    </div>

    <div class="tagsNotification" style="clear:both;">

        <div style="float: left;">
            <asp:Button ID="ClearTags" runat="server" Text="Clear Tags" style="margin-top: 5px;"/>&nbsp;<asp:Button ID="buttonGetTags" runat="server" Text="Suggest Tags" runat="server" style="margin-top: 5px;"/> </div>
        <div class="autoTagsStatus" style="margin-left: 10px; margin-top: 5px; padding: 5px; float: left; display: none;">Waiting...</div>

        <img src="<%= SpinnerImage %>" style="display: none;"/>

    </div>
    <asp:Literal ID="viewHelp" runat="server">
    <div class="tagInstructions" style="width: 500px;clear:both;margin-top:30px;overflow:hidden">
        <p style="line-height:1.4em">To select or deselect tags, <strong>right click</strong> on the tag. Selected (active) tags appear with a <span style="color:white;font-size:11px;font-family:Trebuchet MS,Lucida Grande,verdana,arial;padding:0 4px;background-color:green">green background</span>, deselected (unactive) tags appear with a <span style="font-size:11px;font-family:Trebuchet MS,Lucida Grande,verdana,arial;padding:0 4px;background-color:#ddd">grey background</span>.</p>
    </div>
    </asp:Literal>

<asp:HiddenField ID="ActiveTags" runat="server" />

