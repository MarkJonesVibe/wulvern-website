<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoTagsPostInstall.ascx.cs" Inherits="GrailTechnology.AutoTags.DataType.usercontrols.AutoTagsPostInstall" %>
<p>
	Please select your tagging service provider
</p>
<p>
	Tag Service: <asp:DropDownList ID="ProviderList" runat="server" 
        style="width:350px;" AutoPostBack="true" 
        onselectedindexchanged="ProviderList_SelectedIndexChanged" />
</p>
<asp:Panel ID="pnlOpenCalaisKey" runat="server">
    <p>
        An Open calais API key is required for tag suggestion when using the OpenCalias Tag Provider. You may obtain one from <a href="http://www.opencalais.com/key" target="_blank">opencalais.com</a>
    </p>
    <p>
        Open calais License key: <asp:TextBox ID="CalaisKey" runat="server" style="width: 350px;"></asp:TextBox>
    </p>
</asp:Panel>
<p>
	Please specify the name of the tag Group that you would like to associate tags created using AutoTag with
</p>
<p>
	Tag group name: <asp:TextBox ID="TagGroup" runat="server" style="width:350px;"></asp:TextBox>
</p>
<p>
    <strong>Property Filtering</strong>
</p>
<p>Property aliases (comma seperated: pageTitle,bodyText,keywords etc) you wish to use for tag generation (leave blank for all):</p>
<p><asp:TextBox ID="IncludeAliases" runat="server" style="width:500px;"/></p>
<p>Property aliases (comma seperated: pageTitle,bodyText,keywords etc) you wish to exclude from tag generation:</p>
<p><asp:TextBox ID="ExcludeAliases" runat="server" style="width:500px;"/></p>
<p>
    <asp:Button ID="Button1" runat="server" Text="Save" onclick="Button1_Click" />
    <asp:Literal ID="Literal1" runat="server" Visible="false"><br /><br /><i>Your configuration was saved.</i></asp:Literal>
</p>



