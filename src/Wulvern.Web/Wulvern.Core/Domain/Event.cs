﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wulvern.Core.Domain
{
    public class Event
    {
        public string Title { get; set; }
        public DateTime EventDate { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public Image Image { get; set; }
        public bool DisplayOnWebsite { get; set; }
        public bool DisplayOnIntranet { get; set; }
    }
}
