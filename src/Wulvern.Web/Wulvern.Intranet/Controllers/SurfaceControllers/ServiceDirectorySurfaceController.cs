﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Wulvern.Intranet.Controllers.SurfaceControllers
{
    public class ServiceDirectorySurfaceController : SurfaceController
    {
        public PartialViewResult List( string page = "a" )
        {
          //var productsToShow = this._productsRepository.Products.Where( x => x.ToLower().StartsWith(page) );
          ViewData["currentPage"] = page;
          return PartialView("Companies"); 
        }
    }
}