﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Wulvern.Infrastructure.Models;

namespace Wulvern.Intranet.Controllers.SurfaceControllers
{
    public class DownloadsSurfaceController : SurfaceController
    {
        [ChildActionOnly]
        [HttpGet]
        [ActionName("GetDownloads")]
        public ActionResult Index()
        {
            List<DownloadModel> model = new List<DownloadModel>()
            {
                new DownloadModel()
                {
                    Title = "Download Title",
                    CategoryName = "HR",
                    UploadedBy = "Mjones",
                    FileType = "PDF",
                    DateUploaded = DateTime.Now,
                    Description = "Lorem Ipsum Dolor sit amet."
                },
                new DownloadModel()
                {
                    Title = "Download Title 2",
                    CategoryName = "HR",
                    UploadedBy = "Graham",
                    FileType = "Word Doc",
                    DateUploaded = DateTime.Now.AddDays(-1),
                    Description = "Indocilis privata locqui"
                },
                 new DownloadModel()
                {
                    Title = "Download Title 3",
                    CategoryName = "HR",
                    UploadedBy = "Mjones",
                    FileType = "PDF",
                    DateUploaded = DateTime.Now.AddDays(-3),
                    Description = "Lorem Ipsum Dolor sit amet."
                },
                new DownloadModel()
                {
                    Title = "Download Title 2",
                    CategoryName = "HR",
                    UploadedBy = "Graham",
                    FileType = "Word Doc",
                    DateUploaded = DateTime.Now.AddDays(-4),
                    Description = "Indocilis privata locqui"
                }
            };

            return PartialView("Downloads", model);
        }

        [HttpPost]
        public ActionResult Search()
        {
            return CurrentUmbracoPage();
        }
    }
}