﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Wulvern.Intranet.Helpers
{
    public static class PagerhHelper
    {
        public static MvcHtmlString AlphabeticalPager(this HtmlHelper html, string currentPage, Func<string, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();

            string[] letters = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "Z" };

            foreach (string letter in letters)
            {
                // Construct an <a> tag
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(letter));
                tag.InnerHtml = letter;


                if (letter == currentPage)
                {
                    tag.AddCssClass("highlight");
                }
                result.AppendLine(tag.ToString());
            }

            return MvcHtmlString.Create(result.ToString());
        }
    }
}