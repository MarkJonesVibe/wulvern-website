﻿//using Umbraco.Core;
//using Umbraco.Core.Models;
//using Umbraco.Core.Services;
//using umbraco.BusinessLogic;
//using umbraco.cms.businesslogic.web;
//using System.Collections;
//using System.Collections.Generic;

//namespace Wulvern.Infrastructure.WulvernUmbraco
//{
//    /// <summary>
//    /// Auto move demo
//    /// </summary>
//    public class AutoMoveToArchive : ApplicationBase
//    {
//        /// <summary>
//        /// Wire up the event handler
//        /// </summary>
//        public AutoMoveToArchive()
//        {
//            Document.BeforeSave += new Document.SaveEventHandler(Document_BeforeSave);
//            Document.BeforePublish += new Document.PublishEventHandler(Document_BeforePublish);
//        }


//        /// <summary>
//        /// Before a documents get saved we wil check if the archived proerty is.
//        /// When it's set move it to the archive folder.
//        /// Again it;s demo code, normally you should also check if it's not allready IN the archived foder ;-)
//        /// </summary>
//        void Document_BeforeSave(Document sender, umbraco.cms.businesslogic.SaveEventArgs e)
//        {
//            // Set Arrchive folder id, no excuse to use hard coded values in Umbraco ;-)
//            int archiveId = 1123;
//            //Log that we are doing something
//            Log.Add(LogTypes.Custom, sender.Id, "Document After save Raised");

//            //Check if the item is news and must e archived
//            if (sender.ContentType.Alias == "News" && sender.getProperty("archived").Value.Equals(1))
//            {
//                //Yes, move to archive
//                sender.Move(archiveId);

//                //Unpublish from current Node
//                umbraco.library.UnPublishSingleNode(sender.Id);
//                sender.UnPublish();
//                //Publish will get called if the user selected Save and publish
//            }
//        }

//        // Need to think abou making this method a scheduled task so t doesnt take an event to trigger the action
//        void Document_BeforePublish(Document sender, umbraco.cms.businesslogic.PublishEventArgs e)
//        {
//            // Cast sender to Document type
//            Document doc =(Document)sender;
//            if (doc.ContentType.Alias == "Event")
//            { 
//                /*
//                // Raise alert in umbraco asking user they're sure they want to post the event to the intended destinations
//                if (1 == 1)
//                {
//                    int archiveId = 1155;
//                    //Log that we are doing something
//                    Log.Add(LogTypes.Custom, sender.Id, "Document After save Raised");

//                    //Yes, move to archive
//                    sender.Move(archiveId);

//                    //Unpublish from current Node
//                    umbraco.library.UnPublishSingleNode(sender.Id);
//                    sender.UnPublish();
//                    //Publish will get called if the user selected Save and publish
//                }
//                 */ 
//            }
//            // Check KPI isn't a duplicate
//            if (doc.ContentType.Alias == "KPI")
//            {
//                // Code to check for duplicate KPI properties KPI type, Month , Year
//                var contentService = new Umbraco.Core.Services.ContentService();

//                IEnumerable<IContent> content = contentService.GetChildren(1220);

//                foreach (IContent x in content)
//                {
//                    bool cancel = true;
//                    cancel = (x.GetValue<string>("year") == doc.getProperty("year").Value.ToString() &&
//                        x.GetValue<string>("month") == doc.getProperty("month").Value.ToString() &&
//                        x.GetValue<string>("kPIType") == doc.getProperty("kPIType").Value.ToString());

//                    if (cancel == true)
//                    {
//                        doc.delete();
//                    }
//                }
//            }
//        }
//    }
//}