﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace Wulvern.Infrastructure.Models
{
    public class ShareACommentModel
    {
        public string From { get; set; }
        public string Title { get; set; }
        public string Update { get; set; }
        public Image Photo { get; set; }
        public string Category { get; set; }
    }
}
