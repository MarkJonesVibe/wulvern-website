﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wulvern.Infrastructure.Models
{
    public class EmailSubscriptionModel
    {
        public string EmailAddress { get; set; }
    }
}
