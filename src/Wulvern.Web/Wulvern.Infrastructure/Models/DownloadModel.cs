﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wulvern.Infrastructure.Models
{
    public class DownloadModel
    {
        public string Title { get; set; }
        public Image Image { get; set; } 
        public string CategoryName { get; set; }
        public string UploadedBy { get; set; }
        public string FileType { get; set; }
        public DateTime DateUploaded { get; set; }
        public string Description { get; set; }
    }
}
