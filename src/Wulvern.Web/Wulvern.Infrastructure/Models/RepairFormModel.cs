﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wulvern.Infrastructure.Models
{
    public class RepairFormModel
    {
        public string Tenant { get; set; }
        public string TelephoneNumber { get; set; }
        public string Address { get; set; }
        public string Improvement { get; set; }
        public string ContractorName { get; set; }
        public string ContractorNumber { get; set; }
        public string ContractorAddress { get; set; }
    }
}
