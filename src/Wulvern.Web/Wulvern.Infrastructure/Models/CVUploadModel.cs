﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Wulvern.Infrastructure.Models
{
    public class CVUploadModel
    {
        public string JobId { get; set; }
        public string JobTitle { get; set; }
        public Stream CV { get; set; }
        public string CVName { get; set; }
        public Stream CoverLetter { get; set; }
        public string CoverLetterName { get; set; }

        [Required(ErrorMessage = "*", AllowEmptyStrings = false)]
        [Display(Name="First Name*")]
        public string FirstName { get; set; }

        [Display(Name = "Surname*")]
        [Required(ErrorMessage = "*", AllowEmptyStrings = false)]
        public string Surname { get; set; }

        [Display(Name = "Email*")]
        [Required(ErrorMessage="*")]
        public string Email { get; set; }

        [Display(Name = "Telephone*")]
        [Required(ErrorMessage="*")]
        public string Telephone { get; set; }
    }
}
