﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Wulvern.Infrastructure.Models
{
    public class LinkPodModel
    {
        public string Tilte { get; set; }
        public Image Image { get; set; }
        public string Description { get; set; }
    }
}
